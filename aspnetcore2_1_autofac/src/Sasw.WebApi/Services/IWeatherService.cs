﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sasw.WebApi.Models;

namespace Sasw.WebApi.Services
{
    public interface IWeatherService
    {
        Task<IEnumerable<WeatherForecast>> GetForecasts();
    }
}