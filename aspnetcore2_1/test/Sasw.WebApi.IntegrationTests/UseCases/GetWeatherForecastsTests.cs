﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Sasw.WebApi.IntegrationTests.TestSupport;
using Sasw.WebApi.IntegrationTests.TestSupport.Extensions;
using Sasw.WebApi.Models;
using Sasw.WebApi.Services;
using Xunit;

namespace Sasw.WebApi.IntegrationTests.UseCases
{
    public static class GetWeatherForecastsTests
    {
        public class Given_The_Url_And_Unauthenticated_Client_When_Getting_Weather_Forecasts
            : IntegrationTest
        {
            private HttpResponseMessage _result;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _result = HttpClient.GetAsync("weatherForecast").GetAwaiter().GetResult();
            }

            [Fact]
            public void Then_It_Should_Return_401_Unauthorized()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
            }
        }

        public class Given_The_Url_When_Getting_Weather_Forecasts
            : IntegrationTest
        {
            private HttpResponseMessage _result;
            private string _expectedForecastsJsonResult;

            protected override void Given()
            {
                var username = Configuration.GetValue<string>("BasicAuthentication:Username");
                var password = Configuration.GetValue<string>("BasicAuthentication:Password");
                HttpClient.AddBasicAuthentication(username, password);
                _expectedForecastsJsonResult =
                    "[{\"summary\":\"Sunny\"},{\"summary\":\"Mild\"},{\"summary\":\"Chilly\"}]";
            }

            protected override void When()
            {
                _result = HttpClient.GetAsync("weatherForecast").GetAwaiter().GetResult();
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }

            [Fact]
            public async Task Then_It_Should_Return_Expected_Json()
            {
                var resultAsJson = await _result.Content.ReadAsStringAsync();
                resultAsJson.Should().BeEquivalentTo(_expectedForecastsJsonResult);
            }
        }

        public class Given_The_Url_And_Mock_Weather_Forecast_Service_When_Getting_Weather_Forecasts
            : IntegrationTest
        {
            private HttpResponseMessage _result;
            private string _expectedForecastsJsonResult;

            protected override void ConfigureTestServices(IServiceCollection serviceCollection)
            {
                base.ConfigureTestServices(serviceCollection);
                var weatherServiceMock = new Mock<IWeatherService>();
                weatherServiceMock
                    .Setup(x => x.GetForecasts())
                    .ReturnsAsync(new List<WeatherForecast>
                    {
                        new WeatherForecast("Raining cats and dogs")
                    });

                serviceCollection
                    .AddSingleton<IWeatherService>(sp => weatherServiceMock.Object);
            }

            protected override void Given()
            {
                var username = Configuration.GetValue<string>("BasicAuthentication:Username");
                var password = Configuration.GetValue<string>("BasicAuthentication:Password");
                HttpClient.AddBasicAuthentication(username, password);
                _expectedForecastsJsonResult = "[{\"summary\":\"Raining cats and dogs\"}]";
            }

            protected override void When()
            {
                _result = HttpClient.GetAsync("weatherForecast").GetAwaiter().GetResult();
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }

            [Fact]
            public async Task Then_It_Should_Return_Expected_Json()
            {
                var resultAsJson = await _result.Content.ReadAsStringAsync();
                resultAsJson.Should().BeEquivalentTo(_expectedForecastsJsonResult);
            }
        }
    }
}