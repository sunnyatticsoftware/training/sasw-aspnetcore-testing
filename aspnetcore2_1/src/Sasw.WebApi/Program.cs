﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Sasw.WebApi.Extensions;

namespace Sasw.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost
                .CreateDefaultBuilder(args)
                // It automatically reads from appsettings
                .UseStartup<Startup>();
    }
}
