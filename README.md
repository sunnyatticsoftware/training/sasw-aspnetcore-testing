# Notes

- All the projects target the LTS framework netcoreapp2.1 which requires AspNetCore 2.1 as a separate library
- It uses web host (not generic host).
- The Autofac projects use the `ConfigureContainer` method in Startup that allows void `ConfigureServices` and overwrites with `ConfigureTestContainer`
- The integration test project must contain the same Microsoft.AspNetCore.App version as the web api project it references
- Appsettings are being read and environments are taken into account
- It's possible to overwrite service registrations