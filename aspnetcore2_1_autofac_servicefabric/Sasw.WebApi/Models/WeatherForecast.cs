namespace Sasw.WebApi.Models
{
    public class WeatherForecast
    {
        public string Summary { get; }

        public WeatherForecast(string summary)
        {
            Summary = summary;
        }
    }
}
